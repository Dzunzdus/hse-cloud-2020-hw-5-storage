resource "yandex_compute_instance" "nat" {
  name = "nat"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd89mpdm5r07vhukul89" // nat-instance-ubuntu
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.public-subnet.id
    nat       = true
  }

  metadata = {
    ssh-keys = "altruist:${file("~/.ssh/id_ed25519.pub")}"
  }
}

resource "yandex_vpc_network" "vpc-net" {
  name = "hw2-network"
}

resource "yandex_vpc_route_table" "vpc-route-table" {
  network_id = yandex_vpc_network.vpc-net.id

  static_route {
    destination_prefix = "0.0.0.0/0"
    next_hop_address   = yandex_compute_instance.nat.network_interface.0.ip_address
  }
}

resource "yandex_vpc_subnet" "public-subnet" {
  name           = "public-subnet"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.vpc-net.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "private-subnet" {
  name = "private-subnet"
  zone           = "ru-central1-a"
  v4_cidr_blocks = ["192.168.0.0/24"]
  network_id     = yandex_vpc_network.vpc-net.id
  route_table_id = yandex_vpc_route_table.vpc-route-table.id
}