terraform {
    required_providers {
        yandex = {
            source = "yandex-cloud/yandex"
            version = "0.49.0"
        }
    }
}

provider "yandex" {
    token = "AgAAAAAds8u3AATuwUi8rq-LK0m4oumwr3lPMeM"
    cloud_id = "b1gao5nvuklp7p1a4p2u"
    folder_id = "b1gisah0e9q5lk2on664"
    zone = "ru-central1-a"
}

resource "yandex_compute_instance" "main-1" {
    service_account_id = "aje9qjc79tgjkfllatll"
    name = "main-1"
    hostname = "main-1"
    boot_disk {
        mode = "READ_WRITE"
        initialize_params {
            image_id = data.yandex_compute_image.container-optimized-image.id
        }
    }
    network_interface {
        subnet_id = yandex_vpc_subnet.private-subnet.id
        nat = false
    }
    resources {
        cores = 2
        memory = 2
    }
    metadata = {
        ssh-keys = "altruist:${file("~/.ssh/id_ed25519.pub")}"
    }
    scheduling_policy {
      preemptible = true
    }
}

resource "yandex_compute_instance" "main-2" {
    service_account_id = "aje9qjc79tgjkfllatll"
    name = "main-2"
    hostname = "main-2"
    boot_disk {
        mode = "READ_WRITE"
        initialize_params {
            image_id = data.yandex_compute_image.container-optimized-image.id
        }
    }
    network_interface {
        subnet_id = yandex_vpc_subnet.private-subnet.id
        nat = false
    }
    resources {
        cores = 2
        memory = 2
    }
    metadata = {
        ssh-keys = "altruist:${file("~/.ssh/id_ed25519.pub")}"
    }
    scheduling_policy {
      preemptible = true
    }
}

resource "yandex_compute_instance" "nginx" {
    name = "nginx"
    hostname = "nginx"
    platform_id = "standard-v2"
    service_account_id = "aje9qjc79tgjkfllatll"
    resources {
        cores  = 2
        memory = 2
    }

    boot_disk {
        initialize_params {
            image_id = data.yandex_compute_image.container-optimized-image.id
        }
    }

    network_interface {
        subnet_id = yandex_vpc_subnet.public-subnet.id
        nat       = true
    }

    metadata = {
        ssh-keys = "altruist:${file("~/.ssh/id_rsa.pub")}"
    }
}

resource "yandex_mdb_postgresql_cluster" "managed-psql" {
  name        = "test"
  environment = "PRESTABLE"
  network_id  = yandex_vpc_network.vpc-net.id

  config {
    version = 12
    resources {
      resource_preset_id = "s2.micro"
      disk_type_id       = "network-ssd"
      disk_size          = 16
    }
    postgresql_config = {
      max_connections                   = 395
      enable_parallel_hash              = true
      vacuum_cleanup_index_scale_factor = 0.2
      autovacuum_vacuum_scale_factor    = 0.34
      default_transaction_isolation     = "TRANSACTION_ISOLATION_READ_COMMITTED"
      shared_preload_libraries          = "SHARED_PRELOAD_LIBRARIES_AUTO_EXPLAIN,SHARED_PRELOAD_LIBRARIES_PG_HINT_PLAN"
    }
  }

  database {
    name  = "test"
    owner = "altruist"
  }

  user {
    name       = "altruist"
    password   = "12345678910"
    conn_limit = 50
    permission {
      database_name = "test"
    }
    settings = {
      default_transaction_isolation = "read committed"
      log_min_duration_statement    = 5000
    }
  }

  host {
    zone      = "ru-central1-a"
    subnet_id = yandex_vpc_subnet.private-subnet.id
  }
}

data "yandex_compute_image" "container-optimized-image" {
    family = "container-optimized-image"
}