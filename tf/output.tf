output "nginx_external_ip" {
  value = yandex_compute_instance.nginx.network_interface.0.nat_ip_address
}

output "nginx_internal_ip" {
  value = yandex_compute_instance.nginx.network_interface.0.ip_address
}

output "main-1_internal_ip" {
  value = yandex_compute_instance.main-1.network_interface.0.ip_address
}

output "main-2_internal_ip" {
  value = yandex_compute_instance.main-2.network_interface.0.ip_address
}

output "fqdn" {
  value = yandex_mdb_postgresql_cluster.managed-psql.host.0.fqdn
}
